<?php

namespace App\Http\Controllers;

use App\Models\Correo;
use App\Models\Descuento;
use App\Models\Emisor;
use App\Models\Impuesto;
use App\Models\Receptor;
use App\Models\Servicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\JoinClause;
// use Illuminate\Support\Collection;

class ConsultaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $suma_imp = DB::table('impuestos')
                        ->join('servicios', 'impuestos.id', '=', 'servicios.id')
                        ->join('emisor_receptor_servicio', 'servicios.id', '=', 'emisor_receptor_servicio.servicio_id')
                        ->join('receptors', function ($join){
                            $join->on('receptors.id', '=', 'emisor_receptor_servicio.receptor_id');
                            })
                        ->join('emisors', function ($join){
                            $join->on('emisors.id', '=', 'emisor_receptor_servicio.emisor_id');
                            })
                        ->select('impuestos.tarifa', 'receptors.nombre', DB::raw('SUM(impuestos.monto) as sumatoria'))
                        ->groupBy('receptors.nombre' ,'impuestos.tarifa')
                        ->get();

        $emisores = Emisor::all();
        $receptores = Receptor::all();

        return view('recibido', compact('emisores', 'receptores', 'suma_imp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $servi = DB::table('servicios')
                    ->join('emisor_receptor_servicio', 'servicios.id', '=', 'emisor_receptor_servicio.servicio_id')
                    ->join('impuestos', 'impuestos.id', '=', 'servicios.id')
                    ->join('emisors', function ($join){
                        $join->on('emisors.id', '=', 'emisor_receptor_servicio.emisor_id')
                             ->where('emisors.nombre', '=', request()->em);
                        })
                    ->join('receptors', function ($join){
                        $join->on('receptors.id', '=', 'emisor_receptor_servicio.receptor_id')
                             ->where('receptors.nombre', '=', request()->re);
                        })
                    ->select('servicios.detalle', 'servicios.monto_total', 'impuestos.codigo', 'impuestos.tarifa', 'impuestos.monto')
                    ->get();

        $suma_imp = DB::table('impuestos')
                        ->join('servicios', 'impuestos.id', '=', 'servicios.id')
                        ->join('emisor_receptor_servicio', 'servicios.id', '=', 'emisor_receptor_servicio.servicio_id')
                        ->join('receptors', function ($join){
                            $join->on('receptors.id', '=', 'emisor_receptor_servicio.receptor_id');
                            })
                        ->select('impuestos.tarifa', 'receptors.nombre', DB::raw('SUM(impuestos.monto) as sumatoria'))
                        ->groupBy('receptors.nombre' ,'impuestos.tarifa')
                        ->get();

        $emisores = Emisor::all();
        $receptores = Receptor::all();

        return view('tablacontenido', compact('servi', 'emisores', 'receptores', 'suma_imp'));  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
