<?php

namespace App\Http\Controllers;

use App\Models\Correo;
use App\Models\Adjunto;
use App\Models\Descuento;
use App\Models\Emisor;
use App\Models\Impuesto;
use App\Models\Receptor;
use App\Models\Servicio;
// use App\Models\DocProveedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facade\Storage;
use Storage\logs\laravel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

class MailgunWidgetsController extends Controller
{
    public function store(Request $request){
        app('log')->debug(request()->all());

        $co = Correo::create([
            'fecha' => $request->input('Date'),
            'de' => $request->input('from'),
            'asunto' => $request->input('subject'),
            'para' => $request->input('To'),
            'body' => $request->input('body-plain'),
            'body_html' => $request->input('body-html'),
        ]);

        for ($i=0; $i < intval($request->input('attachment-count')) ; $i++) { 
            $cantidad_att = intval($i)+1; // variable para incremento del i
            $att = 'attachment-'.strval($cantidad_att); // variable que obtiene el nombre de attachment-1 
            $files = $request->file($att)->store('uploads', 'public'); // Almacenar archivo en carpeta storage/uploads
            
            $adj = $co->adjunto()->create([
                        'attachment' => $files,
                        'nombre' => $request->file($att)->getClientOriginalName(), //Obtener el nombre original del archivo xml
                        'tipo' => mime_content_type(storage_path()."/app/public/".$files), // Obtener el tipo de archivo
                    ]);

            // Validación para el proceso de archivos xml
            if ($adj->tipo == 'text/xml') {
                // Leer el xml
                $xmlString = file_get_contents(storage_path()."/app/public/".$files);
                $xmlObject = simplexml_load_string($xmlString);
                $json = json_encode($xmlObject);
                $phpArray = json_decode($json, true);

                $numero_consecutivo = $xmlObject->NumeroConsecutivo;
                $codigo_actividad = $xmlObject->CodigoActividad;
                $fecha = $xmlObject->FechaEmision;

                $em = $xmlObject->Emisor; // Capturar toda la etiqueta emisor
                $id_em = $em->Identificacion; // Capturar toda la etiqueta identificacion dentro de emisor

                // Buscar id de emisor en bd
                $c_emisor = DB::table('emisors')
                            ->where('identificacion', '=', $id_em->Numero)
                            ->value('id');

                // validar si existe identificador del emisor sino insertar
                if ($c_emisor == null) {
                    $modelo_emisor = Emisor::create([
                                        'nombre' => $em->Nombre,
                                        'nombre_comercial' => $em->NombreComercial,
                                        'identificacion' => intval($id_em->Numero),                
                                        'correo' => $em->CorreoElectronico,                                     
                                    ]);
                    $c_emisor = $modelo_emisor->id; 
                }
                
                $re = $xmlObject->Receptor; // Capturar toda la etiqueta receptor
                $id_re = $re->Identificacion; // Capturar toda la etiqueta identificacion dentro de receptor

                // Buscar id de receptor en bd
                $c_receptor = DB::table('receptors')
                                ->where('identificacion', '=', $id_re->Numero)
                                ->value('id');

                // validar si existe identificador del receptor sino insertar
                if ($c_receptor == null) {
                    $modelo_receptor = Receptor::create([
                                        'nombre' => $re->Nombre,
                                        'identificacion' => intval($id_re->Numero),                
                                    ]);
                    $c_receptor = $modelo_receptor->id; 
                }
                
                $ds = $xmlObject->DetalleServicio;

                foreach ($ds->LineaDetalle as $ld) {
                    
                    $serv = Servicio::create([
                        'numero_linea' => intval($ld->NumeroLinea),
                        'codigo' => $ld->Codigo,
                        'cantidad' => intval($ld->Cantidad),
                        'detalle' => $ld->Detalle,
                        'monto' => floatval($ld->MontoTotal),
                        'subtotal' => floatval($ld->SubTotal),
                        'impuesto_neto' => floatval($ld->ImpuestoNeto),
                        'monto_total' => floatval($ld->MontoTotalLinea),
                    ]);                          
                    
                    if(isset($ld->Impuesto)){
                        
                        $imp = $ld->Impuesto;

                        if (!isset($imp->Codigo)) {
                            $cod_imp = null;
                        }else{
                            $cod_imp = $imp->Codigo;
                        }
                        
                        if (!isset($imp->CodigoTarifa)) {
                            $codT_imp = null;
                        }else{
                            $codT_imp = $imp->CodigoTarifa;
                        }
                        
                        $serv->impuesto()->create([
                            'codigo' => $cod_imp,
                            'codigo_tarifa' => $codT_imp,
                            'tarifa' => intval($imp->Tarifa),
                            'monto' =>  floatval($imp->Monto),
                        ]);
                    }

                    if (isset($ld->Descuento)) {
                        $desc = $ld->Descuento;
                        $serv->descuento()->create([
                            'monto' => floatval($desc->MontoDescuento),
                            'naturaleza' => $desc->NaturalezaDescuento,
                        ]);
                    }

                    $serv->receptor()->attach($c_receptor, ['emisor_id' => $c_emisor, 'numero_consecutivo' => $numero_consecutivo, 'codigo_actividad' => $codigo_actividad, 'fecha' => $fecha]);
                }
            }

            
        }    
      return response()->json(['status' => 'ok'], 200);    
    }

    public function index()
    {
        $correos = Correo::all();

        return view('bandeja', compact('correos'));
    }

 /*
    $response = (new Client())->get($file['url'], [
        'auth' => ['api', config('services.mailgun.secret')],
    ]);*/
}
