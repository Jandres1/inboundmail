<?php

namespace App\Http\Controllers;

use App\Models\Correo;
use Illuminate\Http\Request;

class XMLController extends Controller
{
    public function index(Correo $correo)
    {
        $xmlString = file_get_contents(public_path($correo));
        $xmlObject = simplexml_load_string($xmlString);
        
        $json = json_encode($xmlObject);
        $phpArray = json_decode($json, true);

        return view('impuesto', $phpArray);
    }
}
