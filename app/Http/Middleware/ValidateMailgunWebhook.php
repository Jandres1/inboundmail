<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ValidateMailgunWebhook
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected function buildSignature(Request $request)
    {
        return hash_hmac(
            'sha256',
            sprintf('%s%s', $request->input('timestap'), $request->input('token')),
            config('services.mailgun.secret')
        );
    }

    protected function verify(Request $request)
    {
        if (abs(time() - $request->input('timestap')) > 90) {
            return false;
        }

        return $this->buildSignature($request) === $request->input('signature');
    }

    public function handle(Request $request, Closure $next)
    {
        if (!$request->isMethod('post')){
            abort(Response::HTTP_FORBIDDEN, 'Sólo está permitido POST');
        }

        if ($this->verify($request)) {
            return $next($request);
        }

        abort(Response::HTTP_FORBIDDEN);
    }
}