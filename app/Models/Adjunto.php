<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Adjunto extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function correo()
    {
        return $this->belongsTo(Correo::class);
    }
}
