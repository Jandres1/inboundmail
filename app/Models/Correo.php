<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Correo extends Model
{
    use HasFactory;

    /** 
     * The attributes that are mass assignable.
     *
     * @var array
    */
   
    protected $fillable = [
        'fecha',
        'de',
        'asunto',
        'para',
        'body',
        'body_html',
    ];

    public function adjunto()
    {
        return $this->hasMany(Adjunto::class);
    }

}
