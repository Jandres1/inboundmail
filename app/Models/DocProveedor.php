<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocProveedor extends Model
{
    use HasFactory;

    protected $fillable = [
        'cia',
        'fecha_factura',
        'total',
        'total_venta',
        'total_impuestos',
    ];
}
