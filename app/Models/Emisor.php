<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Emisor extends Model
{
    protected $fillable = [
        'nombre',
        'nombre_comercial',
        'identificacion',
        'correo',
    ];

    use HasFactory;

    public function receptor()
    {
        return $this->belongsToMany(Receptor::class, 'emisor_receptor_servicio')
                    ->withPivot('servicio_id', 'status');
    }

    public function servicio()
    {
        return $this->belongsToMany(Servicio::class, 'emisor_receptor_servicio')
                    ->withPivot('receptor_id', 'status');
    }
}
