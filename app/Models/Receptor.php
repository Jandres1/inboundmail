<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receptor extends Model
{   
    protected $fillable = [
        'nombre',
        'identificacion',
    ];

    use HasFactory;

    public function emisor()
    {
        return $this->belongsToMany(Emisor::class, 'emisor_receptor_servicio')
                    ->withPivot('servicio_id', 'status');
    }

    public function servicio()
    {
        return $this->belongsToMany(Servicio::class, 'emisor_receptor_servicio')
                    ->withPivot('emisor_id', 'status');
    }
}
