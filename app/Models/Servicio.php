<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{

    protected $fillable = [
        'numero_linea',
        'codigo',
        'cantidad',
        'detalle',
        'monto',
        'subtotal',
        'impuesto_id',
        'impuesto_neto',
        'monto_total',
    ];

    use HasFactory;

    public function descuento()
    {
        return $this->hasOne(Descuento::class);
    }

    public function impuesto()
    {
        return $this->hasOne(Impuesto::class);
    }

    public function emisor()
    {
        return $this->belongsToMany(Emisor::class, 'emisor_receptor_servicio')
                    ->withPivot('receptor_id', 'status');
    }

    public function receptor()
    {
        return $this->belongsToMany(Receptor::class, 'emisor_receptor_servicio')
                    ->withPivot('emisor_id', 'status');
    }
}
