<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Función hecha para que cuando se cree un usuario nuevo automáticamente se cree su perfil
    protected static function boot()
    {
        parent::boot();

        static::created(
            
        );
    }

    public function posts()
    {
        return $this->hasMany(Post::class)->orderBy('created_at', 'DESC'); // Se utiliza hasMany para una relación de uno a muchos 
    }

    public function profile() //Función que hace la relación con el modelo user
    {
        return $this->hasOne(Profile::class);
    }
}
