<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocProveedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_proveedors', function (Blueprint $table) {
            $table->id();
            $table->string('cia');
            $table->dateTime('fecha_factura');
            $table->double('total', 8, 2);
            $table->double('total_venta', 8, 2);
            $table->double('total_impuestos', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_proveedors');
    }
}
