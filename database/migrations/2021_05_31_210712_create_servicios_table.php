<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
            $table->id();
            $table->integer('numero_linea');
            $table->string('codigo');
            $table->integer('cantidad');
            $table->string('detalle');
            $table->double('monto', 8, 2);
            $table->double('subtotal', 8, 2);
            $table->double('impuesto_neto', 8, 2);
            $table->double('monto_total', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios');
    }
}
