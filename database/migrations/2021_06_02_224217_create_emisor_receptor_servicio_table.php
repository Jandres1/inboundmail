<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmisorReceptorServicioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emisor_receptor_servicio', function (Blueprint $table) {
            $table->id();
            $table->string('numero_consecutivo');
            $table->integer('codigo_actividad');
            $table->string('fecha');
            $table->bigInteger('emisor_id')->unsigned();
            $table->bigInteger('receptor_id')->unsigned();
            $table->bigInteger('servicio_id')->unsigned();
            $table->timestamps();

            $table->foreign('emisor_id')->references('id')->on('emisors')->onDelete('cascade');
            $table->foreign('receptor_id')->references('id')->on('receptors')->onDelete('cascade');
            $table->foreign('servicio_id')->references('id')->on('servicios')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emisor_receptor_servicio');
    }
}
