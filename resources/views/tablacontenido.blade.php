@extends('recibido')

@section('tabla')
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col"> Productos </th>
            <th scope="col"> Monto Producto </th>
            <th scope="col"> Impuesto </th>
            <th scope="col"> Arancel - Tasa </th>
            <th scope="col"> Monto del Impuesto </th>
        </tr>
        </thead>
        <tbody>
            @foreach ($servi as $serv)
                <tr>
                    <th> {{ $serv->detalle }} </th>
                    <td> {{ $serv->monto_total }} </td>
                    <td> Impuesto # {{ $serv->codigo }} </td>
                    <td> {{ $serv->tarifa }} % </td>
                    <td> {{ $serv->monto }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection