<?php

use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'mailgun',
   // 'middleware' => ['mailgun.webhook'],
],function () {
    Route::post('widget', [App\Http\Controllers\MailgunWidgetsController::class, 'store']);
});

/*
Route::group([
    'prefix' => 'mailgun',
], function(){
    Route::get('detail/{id}', [App\Http\Controllers\MailgunWidgetsController::class, 'show']);
});
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});