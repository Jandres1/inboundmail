<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/bandeja', [App\Http\Controllers\MailgunWidgetsController::class, 'index'])->name('bandeja');
Route::get('/impuesto/{correo}', [App\Http\Controllers\XMLController::class, 'index'])->name('impuesto');

Route::get('/consulta', [App\Http\Controllers\ConsultaController::class, 'index'])->name('consulta');
Route::post('/escoger', [App\Http\Controllers\ConsultaController::class, 'show'])->name('escoger');

Auth::routes();
